# Literate Programming

Literate programming means interweaving code, text and graphics to prepare a human and machine readable document. This R notebook is an example.

This is a markdown cell. Markdown is a superset of html, and can be used to generate pretty text.

Because this is R, we'll need to 'knit' the document in order to render the markdown.

```{r}
# This is an R cell. R markdown can have other kernels as well

print("hello world")
```


We are going to do a quick analysis of the iris data set from R. We don't need to install anything now, because these are built-in to R.

We'll just print the first few rows of the data set.


```{r}
head(iris)

```


We can also make nice plots. First we need to load the ggplot library. I also want to use the 'pipe' from maggritr, so I'll just load tidyverse.
```{r}
library(tidyverse)
```

And now just make a plot

```{r}
iris %>% ggplot(aes(x=Sepal.Width, y = Sepal.Length, color=Species)) +
    geom_point() 

```


    

```{r}
iris %>% ggplot(aes(x=Sepal.Width, y = Sepal.Length, color=Species)) +
    geom_smooth() +geom_point()

```

    

BTW: ggplot is still fun!
